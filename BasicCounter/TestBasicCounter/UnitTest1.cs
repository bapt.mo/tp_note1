﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace TestBasicCounter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDecrement()
        {
            Assert.AreEqual(-1, BasicCounterLib.Counter.Decrementation());
        }
        
        [TestMethod]
        public void TestIncrement()
        {
            Assert.AreEqual(0, BasicCounterLib.Counter.Incrementation());
        }
        [TestMethod]
      
        public void TestRAZ()
        {
            Assert.AreEqual(0, BasicCounterLib.Counter.RAZ());
        }

    }
}
