﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class Counter
    {
        public static int x = 0;
        public static int Incrementation()
        {
            x = x + 1;
            return x;
        }

        public static int Decrementation()
        {
            x = x - 1;
            return x;
        }

        public static int RAZ()
        {
            x = 0;
            return x;
        }
    }
}
