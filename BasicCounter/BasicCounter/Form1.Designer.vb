﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.L_Total = New System.Windows.Forms.Label()
        Me.B_Substract = New System.Windows.Forms.Button()
        Me.B_Add = New System.Windows.Forms.Button()
        Me.L_Result = New System.Windows.Forms.Label()
        Me.B_Reset = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'L_Total
        '
        Me.L_Total.AutoSize = True
        Me.L_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Total.Location = New System.Drawing.Point(147, 23)
        Me.L_Total.Name = "L_Total"
        Me.L_Total.Size = New System.Drawing.Size(51, 24)
        Me.L_Total.TabIndex = 0
        Me.L_Total.Text = "Total"
        '
        'B_Substract
        '
        Me.B_Substract.Location = New System.Drawing.Point(43, 69)
        Me.B_Substract.Name = "B_Substract"
        Me.B_Substract.Size = New System.Drawing.Size(75, 23)
        Me.B_Substract.TabIndex = 1
        Me.B_Substract.Text = "-"
        Me.B_Substract.UseVisualStyleBackColor = True
        '
        'B_Add
        '
        Me.B_Add.Location = New System.Drawing.Point(230, 69)
        Me.B_Add.Name = "B_Add"
        Me.B_Add.Size = New System.Drawing.Size(75, 23)
        Me.B_Add.TabIndex = 2
        Me.B_Add.Text = "+"
        Me.B_Add.UseVisualStyleBackColor = True
        '
        'L_Result
        '
        Me.L_Result.AutoSize = True
        Me.L_Result.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Result.Location = New System.Drawing.Point(159, 61)
        Me.L_Result.Name = "L_Result"
        Me.L_Result.Size = New System.Drawing.Size(30, 31)
        Me.L_Result.TabIndex = 3
        Me.L_Result.Text = "0"
        '
        'B_Reset
        '
        Me.B_Reset.Location = New System.Drawing.Point(135, 106)
        Me.B_Reset.Name = "B_Reset"
        Me.B_Reset.Size = New System.Drawing.Size(75, 23)
        Me.B_Reset.TabIndex = 4
        Me.B_Reset.Text = "RAZ"
        Me.B_Reset.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(352, 163)
        Me.Controls.Add(Me.B_Reset)
        Me.Controls.Add(Me.L_Result)
        Me.Controls.Add(Me.B_Add)
        Me.Controls.Add(Me.B_Substract)
        Me.Controls.Add(Me.L_Total)
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents L_Total As Label
    Friend WithEvents B_Substract As Button
    Friend WithEvents B_Add As Button
    Friend WithEvents L_Result As Label
    Friend WithEvents B_Reset As Button
End Class
