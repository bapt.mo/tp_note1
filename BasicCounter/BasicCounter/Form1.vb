﻿Imports BasicCounterLib

Public Class Form1
    Private Sub B_Add_Click(sender As Object, e As EventArgs) Handles B_Add.Click
        L_Result.Text = Counter.Incrementation()

    End Sub

    Private Sub B_Substract_Click(sender As Object, e As EventArgs) Handles B_Substract.Click
        L_Result.Text = Counter.Decrementation()
    End Sub

    Private Sub B_Reset_Click(sender As Object, e As EventArgs) Handles B_Reset.Click
        L_Result.Text = Counter.RAZ()
    End Sub
End Class
